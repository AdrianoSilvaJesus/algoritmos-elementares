/*
By: Adriano Jesus
Date: 14/06/2019
github:https://github.com/AdrianoSilvaOliveiradeJesus
*/

#include <cstdlib>
#include <iostream>

using namespace std;

int main()
{	
	/*
		C = Capacidade do Reservatório
		V = Vasão da Bomba
		Ts = Total de segundos
		R = Resto da divisão
		Qh = Quantida de Horas
		Qm = Quantidade de minutos
		Qs = Qauntidade de segundos
	*/
	int C,V,Ts,R,Qh,Qm,Qs;
	cout << "Entre a capacidade do reservatório: ";
	cin >> C;
	cout << "Entre a vasão da bomba: ";
	cin >> V;
	Ts = C/V;
	Qh = Ts/3600;
	R = Ts%3600;
	Qm = R/60;
	Qs = R%60;
	cout << "Horas = " << Qh << endl;
	cout << "Minutos = " << Qm << endl;
	cout << "Segundos = " << Qs << endl;
	return 0;
}