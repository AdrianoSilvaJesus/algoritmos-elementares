/*
By: Adriano Jesus
Date: 16/06/2019
github:https://github.com/AdrianoSilvaOliveiradeJesus
*/

#include <cstdlib>
#include <iostream>

using namespace std;

int main(){
	/*
		Qped = Quantidade solicitada
		Qext = Quantidade a ser Extraida
	*/
	float Qped,Qext;
	cout << "Quantidade solicitada: ";
	cin >> Qped;
	Qext = Qped/(1-0.02-0.03*0.98);
	cout << "Quantidade a ser extraida: " << Qext << endl;
	return 0;
}