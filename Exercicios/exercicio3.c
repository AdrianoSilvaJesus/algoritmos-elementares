/*
By: Adriano Jesus
Date: 16/06/2019
github:https://github.com/AdrianoSilvaOliveiradeJesus
*/

#include <cstdlib>
#include <iostream>
#include <cmath>

using namespace std;

int main(){
	/*
		VC = Valor Cobrado
		VF = Valor Fornecido
		VT = Valor do Troco
	*/
	float VC,VF,VT;
	/*
		VTc = Valor do troco em centavos
		QM100 = Quantidade de moedas de 1 real
		QM50 = Quantidade de moedas de 50 centavos
		QM10 = Quantidade de moedas de 10 centavos
		QM1 = Quantidade de moedas de 1 centavo
	*/
	int VTc,QM100,QM50,QM10,QM1,R;
	cout << "Entre o valor fornecido: ";
	cin >> VF;
	cout << "Entre o valor cobrado: ";
	cin >> VC;
	VT = VF - VC;
	cout << "Valor do troco: R$" << VT << endl;
	VTc = round(VT*100);
	QM100 = VTc/100;
	R = VTc%100;
	QM50 = R/50;
	R = R%50;
	QM10 = R/10;
	QM1 = R%10;
	cout << "\nMoedas de 1 Real: " << QM100 << endl;
	cout << "Moedas de 50 centavos: " << QM50 <<endl;
	cout << "Moedas de 10 centavos: " << QM10 << endl;
	cout << "Moedas de 1 centavo: " << QM1 << endl;
	return 0;
}