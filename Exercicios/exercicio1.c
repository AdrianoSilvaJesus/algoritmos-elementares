/*
By: Adriano Jesus
Date: 14/06/2019
email:https://github.com/AdrianoSilvaOliveiradeJesus
*/

#include <cstdlib>
#include <iostream>

using namespace std;

int main()
{
	/*
		L = Lucro desejado
		N = Numero de Litros
	*/
	float L,N;
	cout << "Entre o valor do lucro: ";
	cin >> L;
	N = (L+80000)/4;
	cout << "Quantidade de litros a ser produzida: " << N << endl;

	return 0;
}