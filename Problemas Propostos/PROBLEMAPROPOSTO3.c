/*
By: Adriano Jesus
Date: 07/09/2019
github:https://github.com/AdrianoSilvaOliveiradeJesus
*/

#include <cstdlib>
#include <iostream>
#include <cmath>

using namespace std;

int main(){
	/*
		RTALT = Altura da Placa
		RTLARG = Largura da Placa
		RTAREA = Área da Placa
		QDALT = Altura da Placa
		QDLARG = Largura da Placa
		QDAREA = Área da Placa 
		QTQUAD = Quantidade de Quadrados
	*/
	float RTALT,RTLARG,RTAREA,QDALT,QDLARG,QDAREA,QTQUAD;
	cout << "Entre Altura da Placa: ";cin >> RTALT;
	cout << "Entre Largura da Placa: ";cin >> RTLARG;
	cout << "=======================================" << endl;
	RTAREA = RTALT * RTLARG;
	cout << "Entre Altura do Quadrado: ";cin >> QDALT;
	cout << "Entre Largura do Quadrado: ";cin >> QDLARG;
	QDAREA = QDALT * QDLARG;
	QTQUAD = RTAREA/QDAREA;
	cout << "Quantidade de Quadrados: " << trunc(-25.4) << endl;
	return 0;
}