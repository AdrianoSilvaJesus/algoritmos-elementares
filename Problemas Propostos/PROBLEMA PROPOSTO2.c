#include <cstdlib>
#include <iostream>

using namespace std;

int main()
{
	int Xa,Ya,Xb,Yb,Altura,Base,Area;
	/*Obtendo valores da diagonal (Xa,Ya) e (Xb,Yb)*/
	cout << "Entre Xa: ";cin >> Xa;
	cout << "Entre Ya: ";cin >> Ya;
	cout << "Entre Xb: ";cin >> Xb;
	cout << "Entre Yb:  ";cin >> Yb;
	/*Calculando a diferença de Altura entre "Yb e Ya*/
	Altura = Yb-Ya;
	/*Calculando a distância entre "Xb e Xa"*/
	Base = Xb - Xa;
	/*Calculando a área do retângulo "Base x Altura*/
	Area = Base*Altura;
	cout << "A área do retângulo é: " << Area << "Cm2";
	return 0;
}