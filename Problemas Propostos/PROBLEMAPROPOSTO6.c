#include <iostream>
#include <cstdlib>

using namespace std;

int main(){
	/*
		C = Comprimento da Barra
		C1 = Distância C1
		C2 = Distância C2
		F1 = Força F1
		F2 = Força F2
		M1 = Movimento 1
		M2 = Movimento 2
	*/
	float C,C1,C2,F1,F2,M1,M2;
	cout << "Entre Comprimento da Barra: "; cin >> C; 
	cout << "Entre F1: "; cin >> F1;
	cout << "Entre F2: "; cin >> F2;
	/*Fórmula: F1.(C-C2) = F2.C2*/
	/*Aplica-se distributiva*/
	M1 = F1*C;
	/*Distância com distância*/
	M2 = F2+F1;
	C2 = M1/M2;
	/*Diferença entre o comprimento da barra e a distância C2 resulta em "C1"*/
	C1 = C-C2;
	cout << "Distância C1: " << C1 << endl;
	cout << "Distância C2: " << C2 << endl;
	return 0;
}