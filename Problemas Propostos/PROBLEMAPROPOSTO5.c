/*
By: Adriano Jesus
Date: 08/09/2019
github:https://github.com/AdrianoSilvaOliveiradeJesus
*/

#include <cstdlib>
#include <iostream>

using namespace std;

int main(){
	/*
		R = Resto da Divisão
		CX40 = Quantidade de Caixas com 40 Parafusos
		CX10 = Quantidade de Caixas com 10 Parafusos
		Sobra = Quantidade de Parafusos que Não Serão Embalados
		QTPARAFUSO = Quantidade de Parafusos
	*/
	int R,CX40,CX10,SOBRA,QTPARAFUSO;
	cout << "Entre Quantidade de Parafusos: ";cin >> QTPARAFUSO;
	CX40 = QTPARAFUSO/40;
	R = QTPARAFUSO%40;
	CX10 = R/10;
	SOBRA = R%10;
	cout << "Caixas de 40: " << CX40 << endl;
	cout << "Caixas de 10: " << CX10 << endl;
	cout << "Quantidade de Parafusos Não Embalados: " << SOBRA << endl;
	return 0;
}