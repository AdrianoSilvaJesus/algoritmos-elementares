/*
By: Adriano Jesus
Date: 01/09/2019
github:https://github.com/AdrianoSilvaOliveiradeJesus
*/


#include <cstdlib>
#include <iostream>

using namespace std;

int main()
{
	float Xa,Ya,Xb,Yb,Distancia,Circunferencia;
	/*Coleta de Dados Iniciais*/
	cout << "Entre Xa: ";cin >> Xa;
	cout << "Entre Ya: ";cin >> Ya;
	cout << "Entre Xb: ";cin >> Xb;
	cout << "Entre Yb: ";cin >> Yb;
	/*Distancia entre os dois pontos do plano cartesiano*/
	Distancia = Xb-Xa;
	/*Calculo do comprimento da circunferência 3,14 x Diametro*/
	Circunferencia = 3.14*Distancia;
	cout << "\nTamanho da Circunferência " << Circunferencia << endl;
}