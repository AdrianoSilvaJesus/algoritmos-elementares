/*
By: Adriano Jesus
Date: 08/09/2019
github:https://github.com/AdrianoSilvaOliveiradeJesus
*/

#include <cstdlib>
#include <iostream>

using namespace std;

int main(){
	/*
		MS = Velocidade em metros por segundo
		CONVERTKH = Converte M/S em KM/H usando a fórmula (M/S*3,6)
	*/
	float MS,CONVERTKH;
	cout << "Entre velocidade M/S: ";cin >> MS;
	CONVERTKH = MS*3.6;
	cout << CONVERTKH << " KM/H" << endl;	
	return 0;
}